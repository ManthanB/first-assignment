import datetime
import sys


class Activity(object):
	def __init__ (self, user_id, duration, rotation, calories, date=datetime.date(2015, 2, 22), month=datetime.date.month):
		self.user_id = user_id
		self.duration = duration
		self.rotation = rotation
		self.calories = calories
		self.date = date
		

def daily_aggregation(arc):
	agg = {}
	for a in arc:
		if a.user_id not in agg.iterkeys():
			agg[a.user_id]={a.date:{'rotation':a.rotation, 'calories':a.calories}}
		#	print agg
		else:
			if a.date not in agg[a.user_id].keys():
				agg[a.user_id][a.date] = {'rotation': a.rotation, 'calories': a.calories}
			else:
				agg[a.user_id][a.date]['rotation'] += a.rotation
				agg[a.user_id][a.date]['calories'] += a.calories
		#print agg

	return agg


def weekly_aggregation(arc):
	agg = {}
	aggkey =arc[1].date.strftime("%Y%W");
	for a in arc:
		aggKey=a.date.strftime("%Y%W")
		if a.user_id not in agg.iterkeys():
			agg[a.user_id]={aggkey:{'rotation':a.rotation, 'calories':a.calories}}
		#	print agg
		else:
			if aggKey not in agg[a.user_id].keys():
				agg[a.user_id][aggKey] = {'rotation': a.rotation, 'calories': a.calories}
						
			else:

				agg[a.user_id][aggKey]['rotation'] += a.rotation
				agg[a.user_id][aggKey]['calories'] += a.calories
		#print agg

	return agg


def monthly_aggregation(arc):
	agg = {}
	aggKey=arc[1].date.strftime("%Y-%m");
	for a in arc:
		aggKey=a.date.strftime("%Y-%m")
		if a.user_id not in agg.iterkeys():
			agg[a.user_id]={aggKey:{'rotation':a.rotation, 'calories':a.calories}}
		#	print agg
		else:
			if aggKey not in agg[a.user_id].keys():
				agg[a.user_id][aggKey] = {'rotation': a.rotation, 'calories': a.calories}
						
			else:

				agg[a.user_id][aggKey]['rotation'] += a.rotation
				agg[a.user_id][aggKey]['calories'] += a.calories
		#print agg

	return agg

def yearly_aggregation(arc):
	agg = {}
	aggKey=arc[1].date.strftime("%Y")
	for a in arc:
		aggKey=a.date.strftime("%Y")
		if a.user_id not in agg.iterkeys():
			agg[a.user_id]={aggKey:{'rotation':a.rotation, 'calories':a.calories}}
		#	print agg
		else:
			if aggKey not in agg[a.user_id].keys():
				agg[a.user_id][aggKey] = {'rotation': a.rotation, 'calories': a.calories}
						
			else:

				agg[a.user_id][aggKey]['rotation'] += a.rotation
				agg[a.user_id][aggKey]['calories'] += a.calories
		#print agg

	return agg

	'''	if type(self.date) != datetime.date:
			print'nvalid date. Specify proper date.'
			sys.exit(1)'''

