from fitness import Activity,weekly_aggregation, monthly_aggregation, yearly_aggregation

import datetime

actw1=Activity(1,10,10,10,datetime.date(2015,2,2))

actw2=Activity(1,10,10,10,datetime.date(2015,2,3))

actw3=Activity(1,10,10,10,datetime.date(2015,2,4))

week=weekly_aggregation([actw1,actw2,actw3])

print "Weekly Aggregation = " , week

actw4=Activity(1,10,10,10,datetime.date(2015,2,4))

actw5=Activity(1,10,10,10,datetime.date(2015,2,14))

actw6=Activity(1,10,10,10,datetime.date(2015,2,24))

month=monthly_aggregation([actw4,actw5,actw6])

print "Monthly Aggregation = " , month

actw7=Activity(1,10,10,10,datetime.date(2015,2,24))

actw8=Activity(1,10,10,10,datetime.date(2015,3,24))

actw9=Activity(1,10,10,10,datetime.date(2015,4,24))

year = yearly_aggregation([actw7,actw8,actw9])

print "Yearly Aggregation = " , year