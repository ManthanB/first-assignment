s="hello, how r u, good morning, this, is demo, of, string split, try, this, with, python, shell"

print "Direct Split:"
print s.split(',')

print "Split Only 2:"
print s.split(',',2)

print "Split Only 3:"
print s.split(',',3)

print "Split in 4:"
print s.split(',',4)